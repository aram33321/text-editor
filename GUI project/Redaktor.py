from tkinter import *
from tkinter.filedialog import *

def Exit_file(event):
    global root
    root.destroy()
    
def Open_file(event): 
    function = Open(root, filetypes = [("All files", "*.*")]).show()
    if function == '':
        return
    textbox.delete('1.0', 'end') 
    textbox.insert('1.0', open(function, 'rt').read())
    
def SaveAs_file(event):
    function = SaveAs(root).show()
    if function == '':
        return
    open(function, 'wt').write(textbox.get('1.0', 'end'))

root = Tk()
root.title("Redaktor")

panel = Frame(root, height = 60, bg = 'gainsboro')
text = Frame(root, height = 600, width = 600)

panel.pack(side = 'top', fill = 'x')
text.pack(side = 'bottom', fill = 'both', expand = 1)

textbox = Text(text, font='Arial 12', wrap='word')
scrollbar = Scrollbar(text)

scrollbar['command'] = textbox.yview
textbox['yscrollcommand'] = scrollbar.set

textbox.pack(side = 'left', fill = 'both', expand = 1)
scrollbar.pack(side = 'right', fill = 'y')

open_button = Button(root, text = 'Open')
saveas_button = Button(root, text = 'SaveAs')
exit_button = Button(root, text = 'Exit')

open_button.bind("<Button-1>", Open_file)
saveas_button.bind("<Button-1>", SaveAs_file)
exit_button.bind("<Button-1>", Exit_file)

open_button.place(x = 10, y = 10, width = 70, height = 40)
saveas_button.place(x = 90, y = 10, width = 70, height = 40)
exit_button.place(x = 170, y = 10, width = 70, height = 40)

root.mainloop()